package com.nimbusds.oauth2.sdk;


/**
 * Response message indicating success.
 *
 * @author Vladimir Dzhuvinov
 */
public interface SuccessResponse extends Response { }
